use bson::Bson;
use mongodb::{Client, ThreadedClient,ClientInner};
use mongodb::db::{ThreadedDatabase};
use std::sync::Arc;
use iron;

#[derive(Copy, Clone)]
pub struct Mongo;
// static client: Arc<ClientInner> = Client::connect("localhost", 27017).ok().expect("Failed to initialize standalone client.");

impl iron::typemap::Key for Mongo {
    type Value = Client;
}
