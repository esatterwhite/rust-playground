use iron::prelude::*;
use iron::status;
use tera::{Tera,Context};
use std::path::Path;
use iron::headers::ContentType;
use hyper::mime::{Mime, TopLevel, SubLevel, Attr, Value};
use iron::modifiers::Header;
use persistent::State;

use mongo::Mongo;

pub fn homepage(req: &mut Request) -> IronResult<Response> {	
	let mut context = Context::new();
	
	let pool = &req.get::<State<Mongo>>().unwrap();
	let coll = pool.db("test").collection("movies");

	let doc = doc! { "title" => "Jaws",
	                  "array" => [ 1, 2, 3 ] };

	// Insert document into 'test.movies' collection
	coll.insert_one(doc.clone(), None)
	    .ok().expect("Failed to insert document.");


	// Use globbing
    let tera = Tera::new("core/templates/**/*");
	context.add("name", &"World");
	let page = tera.render("index.html", context).unwrap();
	let mut resp = Response::with((status::Ok, page ));
	resp.headers.set( ContentType::html() );
	Ok(resp)
}