
#[macro_use(bson, doc)]
extern crate bson;
#[macro_use]
extern crate router;
extern crate mongodb;
extern crate iron;
extern crate hyper;
extern crate staticfile;
extern crate persistent;
	

extern crate mount;
extern crate tera;

use mongodb::{Client, ThreadedClient};
use mongodb::db::ThreadedDatabase;

mod mongo;
mod core;


use std::vec::Vec;
use iron::modifiers::Redirect;
use iron::headers::{CacheControl, CacheDirective};
use iron::prelude::*;
use iron::status;
use mount::Mount;
use mongo::Mongo;

use core::views::home::homepage;

fn main() {
    let mut mount = Mount::new();
    mount.mount("/", homepage);

    let mut chain = iron::Chain::new(mount);
    chain.link(persistent::State::<Mongo>::both(
            Client::connect("localhost", 27017).unwrap()
        )
    );
    // And start serving those routes
    // On port `5000` of all interfaces
    Iron::new(chain).http("0.0.0.0:5000").unwrap();
}
